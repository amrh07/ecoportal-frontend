import "../styles/globals.css";
import type { AppProps } from "next/app";
import React, { FC, useState } from "react";
import { Provider as ReduxProvider } from "react-redux";
import Head from "next/head";
import { createStore } from "../redux";
import { EnhancedStore } from "@reduxjs/toolkit";
import {
  ApolloClient,
  InMemoryCache,
  ApolloProvider,
  createHttpLink,
} from "@apollo/client";
import { ThemeProvider, createTheme } from "@mui/material/styles";

const App: FC<AppProps> = ({ Component, pageProps }) => {
  const [store, setStore] = useState<EnhancedStore | null>(null);
  React.useEffect(() => {
    const client = new ApolloClient({
      cache: new InMemoryCache(),
      uri: "http://localhost:5001/graphql",
    });

    const store = createStore({ epicDependencies: { client } });
    setStore(store);
  }, []);

  const link = createHttpLink({
    uri: "/graphql",
    credentials: "same-origin",
  });

  const client = new ApolloClient({
    cache: new InMemoryCache(),
    link,
  });
  const theme = createTheme({
    palette: {
      primary: {
        main: "#002855",
      },
      secondary: {
        main: "#33415c",
      },
      background: {
        paper: "#f1f7ff",
      },
    },
  });

  if (!store) return <>{"Loading..."}</>;
  return (
    <>
      <Head>
        <title>{"Coolmovies Frontend"}</title>
        <meta charSet="UTF-8" />
        <meta httpEquiv="X-UA-Compatible" content="IE=edge" />
        <meta name="viewport" content="width=device-width, initial-scale=1.0" />
      </Head>
      <ApolloProvider client={client}>
        <ReduxProvider store={store}>
          <ThemeProvider theme={theme}>
            <Component {...pageProps} />
          </ThemeProvider>
        </ReduxProvider>
      </ApolloProvider>
    </>
  );
};

export default App;
