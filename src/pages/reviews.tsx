import { useEffect, useState } from "react";
import { NextPage } from "next";
import { Typography, Box, Container, Button } from "@mui/material";
import { useMemo } from "react";
import { gql, useQuery } from "@apollo/client";
import { css } from "@emotion/react";
import MovieCard from "../components/MovieCard";
import FormDialog from "../components/FormDialog";

const allMovieReviewsById = gql`
  query allMovieReviews {
    allMovieReviews {
      nodes {
        title
        body
        rating
        nodeId
        movieByMovieId {
          id
          title
          imgUrl
          userByUserCreatorId {
            id
            name
          }
        }
        commentsByMovieReviewId {
          nodes {
            id
            title
            body
            userByUserId {
              id
              name
            }
          }
        }
      }
    }
  }
`;

type MovieType = {
  movieByMovieId: {
    imgUrl: string;
    title: string;
    userByUserCreatorId: {
      name: string;
    };
  };
  title: string;
  rating: number;
  body: string;
  nodeId: string;
};

const Reviews: NextPage = () => {
  const [openNewReview, setOpenNewReview] = useState(false);
  const { loading, data } = useQuery(allMovieReviewsById, {
    pollInterval: 3000,
  });

  useEffect(() => {}, [data]);

  const handleOpenNewReview = () => {
    setOpenNewReview(true);
  };

  const handleCloseNewReview = () => {
    setOpenNewReview(false);
  };

  const movies = useMemo(() => data?.allMovieReviews.nodes, [data]);

  if (loading) return <p>Loading...</p>;
  return (
    <Box css={styles.root}>
      <FormDialog open={openNewReview} onClose={handleCloseNewReview} />
      <Typography
        variant="h4"
        component="h1"
        align="center"
        gutterBottom
        sx={{ color: "#4f5d75" }}
      >
        Reviews
      </Typography>
      <Button onClick={() => handleOpenNewReview()} css={styles.addReviewBtn}>
        Add new review
      </Button>
      <Container css={styles.movieCardsContainer}>
        {movies.map((movie: MovieType) => {
          return (
            <MovieCard
              imgUrl={movie.movieByMovieId.imgUrl}
              reviewTitle={movie.title}
              movieTitle={movie.movieByMovieId.title}
              author={movie.movieByMovieId.userByUserCreatorId.name}
              score={movie.rating}
              key={movie.title}
              review={movie.body}
              nodeId={movie.nodeId}
            />
          );
        })}
      </Container>
    </Box>
  );
};

export default Reviews;

const styles = {
  root: css({
    height: "100%",
    width: "100%",
    display: "flex",
    flexDirection: "column",
    alignItems: "center",
    justifyContent: "start",
    backgroundColor: "#f5f3f4",
    padding: "4rem 6rem",
    gap: 6,
    "@media (max-width:1100px)": {
      padding: "2rem 0rem",
    },
  }),
  movieCardsContainer: css({
    display: "flex",
    flexWrap: "wrap",
    alignItems: "flex-start",
    gap: 2,
    padding: "0 100px !important",
    justifyContent: "space-between",
    "@media (max-width: 900px)": {
      justifyContent: "center",
      padding: "0 10px !important",
    },
  }),
  addReviewBtn: css({
    backgroundColor: "#721d94",
    color: "#fff",
    padding: "10px 1rem",
    marginBottom: "1rem",

    "&:hover": {
      background: "#920e7c",
      transition: "background ease-in-out 0.2s",
    },
  }),
};
