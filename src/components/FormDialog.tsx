import React, { useMemo, useState } from "react";
import {
  Button,
  Dialog,
  DialogActions,
  DialogContent,
  DialogTitle,
  FormControl,
  FormControlLabel,
  FormLabel,
  MenuItem,
  Radio,
  RadioGroup,
  Select,
  TextField,
} from "@mui/material";
import { SelectChangeEvent } from "@mui/material/Select";

import { css } from "@emotion/react";
import { useQuery, gql, useMutation } from "@apollo/client";

const allMovies = gql`
  query AllMovies {
    allMovies {
      edges {
        node {
          id
          title
        }
      }
    }
    currentUser {
      id
    }
  }
`;

const CREATE_MOVIE_REVIEW = gql`
  mutation CreateMovieReview($input: CreateMovieReviewInput!) {
    createMovieReview(input: $input) {
      movieByMovieId {
        title
      }
    }
  }
`;

interface FormDialogProps {
  open: boolean;
  onClose: () => void;
}

type MovieShortType = {
  node: {
    id: string;
    title: string;
  };
};

const FormDialog: React.FC<FormDialogProps> = ({ open, onClose }) => {
  const [title, setTitle] = useState("");
  const [body, setBody] = useState("");
  const [rating, setRating] = useState(1);
  const [movie, setMovie] = useState("");

  const { data } = useQuery(allMovies);
  const [createMovieReview, { data: mutationData }] =
    useMutation(CREATE_MOVIE_REVIEW);

  const movies = useMemo(() => {
    return data?.allMovies.edges;
  }, [data]);

  const currentUserId = useMemo(() => {
    return data?.currentUser.id;
  }, [data?.currentUser]);

  const handleTitleChange = (event: React.ChangeEvent<HTMLInputElement>) => {
    setTitle(event.target.value);
  };

  const handleBodyChange = (event: React.ChangeEvent<HTMLInputElement>) => {
    setBody(event.target.value);
  };

  const handleRatingChange = (event: React.ChangeEvent<HTMLInputElement>) => {
    setRating(parseInt(event.target.value));
  };

  const handleMovieChange = (event: SelectChangeEvent) => {
    setMovie(event.target.value as string);
  };

  const handleSubmit = () => {
    createMovieReview({
      variables: {
        input: {
          movieReview: {
            title: title,
            body: body,
            movieId: movie,
            rating: rating,
            userReviewerId: currentUserId,
          },
        },
      },
    });
    onClose();
  };

  return (
    <Dialog open={open} onClose={onClose}>
      <DialogTitle css={styles.dialogTitle}>New review</DialogTitle>
      <DialogContent css={styles.dialog}>
        <TextField
          autoFocus
          margin="dense"
          label="Title"
          value={title}
          onChange={handleTitleChange}
          fullWidth
        />
        <TextField
          margin="dense"
          label="Review"
          value={body}
          onChange={handleBodyChange}
          fullWidth
        />
        <FormControl component="fieldset" style={{ marginTop: "16px" }}>
          <FormLabel component="legend">Rating</FormLabel>
          <RadioGroup
            aria-label="rating"
            name="rating"
            value={rating.toString()}
            onChange={handleRatingChange}
            row
          >
            {[1, 2, 3, 4, 5].map((value) => (
              <FormControlLabel
                key={value}
                value={value.toString()}
                control={<Radio color="primary" />}
                label={value.toString()}
              />
            ))}
          </RadioGroup>
        </FormControl>
        <FormControl fullWidth style={{ marginTop: "16px" }}>
          <FormLabel component="legend">Movie</FormLabel>
          <Select value={movie} onChange={handleMovieChange}>
            {data &&
              movies.map((movie: MovieShortType) => (
                <MenuItem
                  key={`${movie.node.id} - ${Date.now()}`}
                  value={movie.node.id}
                >
                  {movie.node.title}
                </MenuItem>
              ))}
          </Select>
        </FormControl>
      </DialogContent>
      <DialogActions css={styles.dialogTitle}>
        <Button onClick={onClose} color="primary" css={styles.cancelBtn}>
          Cancel
        </Button>
        <Button onClick={handleSubmit} color="primary" css={styles.submitBtn}>
          Submit
        </Button>
      </DialogActions>
    </Dialog>
  );
};

export default FormDialog;

const styles = {
  dialog: css({
    background: "#f5f3f4",
    minWidth: "270px !important",
  }),
  dialogTitle: css({
    background: "#f5f3f4",
    color: "#000",
  }),

  submitBtn: css({
    background: "#4f5d75",
    color: "#f5f3f4",

    "&:hover": {
      background: "#2e3746",
      transition: "background ease-in-out 0.2s",
    },
  }),
  cancelBtn: css({
    background: "#cf1f1f",
    color: "#f5f3f4",

    "&:hover": {
      background: "#b33109",
      transition: "background ease-in-out 0.2s",
    },
  }),
};
