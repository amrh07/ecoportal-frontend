import StarIcon from "@mui/icons-material/Star";
import { css } from "@emotion/react";

const StarRating = ({ score }: { score: number }) => {
  const renderStarts = () => {
    const starts = [];

    for (let i = 0; i < score; i++) {
      starts.push(<StarIcon key={i} fontSize="small" css={styles.star} />);
    }
    return starts;
  };

  return <>{renderStarts()}</>;
};

export default StarRating;

const styles = {
  star: css({
    color: "#FFFFFF",
  }),
};
