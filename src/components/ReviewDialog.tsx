import { useState } from "react";
import { css } from "@emotion/react";
import Button from "@mui/material/Button";
import { styled } from "@mui/material/styles";
import Dialog from "@mui/material/Dialog";
import DialogTitle from "@mui/material/DialogTitle";
import DialogContent from "@mui/material/DialogContent";
import DialogActions from "@mui/material/DialogActions";
import IconButton from "@mui/material/IconButton";
import CloseIcon from "@mui/icons-material/Close";
import Typography from "@mui/material/Typography";
import TextareaAutosize from "@mui/base/TextareaAutosize";
import Image from "next/image";
import { gql, useMutation } from "@apollo/client";

import EditIcon from "../../public/edit.svg";

const UPDATE_REVIEW = gql`
  mutation updateMovieReview($input: UpdateMovieReviewInput!) {
    updateMovieReview(input: $input) {
      movieReview {
        body
      }
    }
  }
`;

const BootstrapDialog = styled(Dialog)(({ theme }) => ({
  "& .MuiDialogContent-root": {
    padding: theme.spacing(2),
  },
  "& .MuiDialogActions-root": {
    padding: theme.spacing(1),
  },
}));

export interface DialogTitleProps {
  id: string;
  children?: React.ReactNode;
  onClose: () => void;
}

function BootstrapDialogTitle(props: DialogTitleProps) {
  const { children, onClose, ...other } = props;

  return (
    <DialogTitle sx={{ m: 0, p: 2 }} {...other}>
      {children}
      {onClose ? (
        <IconButton
          aria-label="close"
          onClick={onClose}
          sx={{
            position: "absolute",
            right: 8,
            top: 8,
            color: (theme) => theme.palette.grey[500],
          }}
        >
          <CloseIcon />
        </IconButton>
      ) : null}
    </DialogTitle>
  );
}

type DialogProps = {
  open: boolean;
  handleClose: () => void;
  review: string;
  title: string;
  nodeId: string;
};

const ReviewDialog = (props: DialogProps) => {
  const [edit, setEdit] = useState(false);
  const [text, setText] = useState(props.review || "");
  const nodeId = props.nodeId;
  const [updateMovieReview] = useMutation(UPDATE_REVIEW);

  const handleEdit = () => {
    setEdit((prevState) => !prevState);
  };

  const handleCloseDialog = () => {
    props.handleClose();
    edit ? handleEdit() : "";
  };

  const handleSubmitUpdate = () => {
    updateMovieReview({
      variables: {
        input: {
          nodeId: nodeId,
          movieReviewPatch: {
            body: text,
          },
        },
      },
    });
    props.handleClose();
    edit ? handleEdit() : "";
  };

  return (
    <div>
      <BootstrapDialog
        onClose={handleCloseDialog}
        aria-labelledby="customized-dialog-title"
        open={props.open}
      >
        <BootstrapDialogTitle
          id="customized-dialog-title"
          onClose={handleCloseDialog}
          css={styles.dialogTitle}
        >
          {props.title}
        </BootstrapDialogTitle>
        <DialogContent dividers css={styles.dialog}>
          {!edit && <Typography gutterBottom>{props.review}</Typography>}
          {edit && (
            <TextareaAutosize
              defaultValue={props.review}
              css={styles.textArea}
              value={text}
              onChange={(e) => {
                setText(e.target.value);
              }}
            />
          )}
        </DialogContent>
        <DialogActions css={styles.dialogTitle}>
          <Button autoFocus onClick={() => handleEdit()} css={styles.editBtn}>
            <Image src={EditIcon} alt="Edit icon" />
          </Button>
          {edit && (
            <Button
              autoFocus
              onClick={() => handleSubmitUpdate()}
              css={styles.saveBtn}
            >
              Save changes
            </Button>
          )}
        </DialogActions>
      </BootstrapDialog>
    </div>
  );
};

export default ReviewDialog;

const styles = {
  dialog: css({
    background: "#f5f3f4",
    minWidth: "270px !important",
  }),
  dialogTitle: css({
    background: "#f5f3f4",
    color: "#000",
  }),
  editBtn: css({
    height: "34px",
    width: "34px",
    padding: "0px !important",
    minWidth: "0px !important",

    img: {
      height: "20px !important",
      width: "20px !important",
    },
  }),
  textArea: css({
    minWidth: "290px",
    "@media (min-width: 600px)": {
      minWidth: "500px",
    },
  }),
  saveBtn: css({
    background: "#4f5d75",
    color: "#f5f3f4",

    "&:hover": {
      background: "#2e3746",
      transition: "background ease-in-out 0.2s",
    },
  }),
};
