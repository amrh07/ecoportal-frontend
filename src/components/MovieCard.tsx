import {
  Card,
  CardContent,
  CardMedia,
  Typography,
  Box,
  Container,
} from "@mui/material";
import StarRating from "./StarRating";
import { css } from "@emotion/react";
import ReviewDialog from "./ReviewDialog";
import { useState } from "react";

export type MovieCardProps = {
  imgUrl: string;
  reviewTitle: string;
  movieTitle: string;
  author: string;
  score: number;
  review: string;
  nodeId: string;
};

const MovieCard = (props: MovieCardProps) => {
  const [open, setOpen] = useState(false);

  const handleOpenDialog = () => {
    setOpen(true);
  };

  const handleCloseDialog = () => {
    setOpen(false);
  };

  return (
    <div>
      <ReviewDialog
        open={open}
        handleClose={handleCloseDialog}
        review={props.review}
        title={props.reviewTitle}
        nodeId={props.nodeId}
      />
      <Card
        css={styles.cardContainer}
        elevation={0}
        onClick={() => handleOpenDialog()}
      >
        <CardMedia
          component="img"
          height="194"
          image={props.imgUrl}
          alt={`${props.movieTitle} image`}
        />
        <CardContent css={styles.cardContent}>
          <Box css={styles.cardDetailsBox}>
            <Container css={styles.titleBox}>
              <Typography variant="h6" component="h2" sx={{ color: "#ffffff" }}>
                {props.reviewTitle}
              </Typography>
              <Typography
                variant="body2"
                color="text.secondary"
                sx={{ color: "#ffffff" }}
              >
                {`By: ${props.author}`}
              </Typography>
            </Container>
            <div>
              <StarRating score={props.score} />
            </div>
          </Box>
        </CardContent>
      </Card>
    </div>
  );
};

export default MovieCard;

const styles = {
  cardContainer: css({
    display: "flex",
    flexDirection: "column",
    alignItems: "start",
    justifyContent: "flex-end",
    margin: "10px",
    width: "300px",
    height: "310px",
    background: "#4f5d75",
    backgroundRepeat: "no-repeat",
    backgroundSize: "cover",
    "&:hover": {
      opacity: 0.8,
      cursor: "pointer",
    },
  }),
  cardDetailsBox: css({
    display: "flex",
    flexDirection: "column",
    justifyContent: "center",
    alignItems: "flex-end",
    width: "100%",
    padding: "0px",
  }),
  titleBox: css({
    display: "flex",
    flexDirection: "column",
    flex: 1,
    alignItems: "flex-start",
    justifyContent: "flex-start",
    width: "100%",
    padding: "0px !important",
  }),
  cardContent: css({
    width: "100%",
    maxHeight: "120px",
  }),
};
